package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.api.models.sacnhistory.ScanHistoryResult;

import java.util.List;


public class ScanHistoryAdapter extends ArrayAdapter {

    Context context;
    List<ScanHistoryResult> data;

    public ScanHistoryAdapter(Context context, List<ScanHistoryResult> data) {
        super(context,R.layout.scanhistory);
        this.context=context;
        this.data=data;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView=null;
        if (convertView==null){

            convertView = LayoutInflater.from(context).inflate(R.layout.scanhistory, null);

            TextView datehistory =convertView.findViewById(R.id.datehistory);
            TextView pnumberhistory =convertView.findViewById(R.id.pnumberhistory);
            TextView scanresulthistory =convertView.findViewById(R.id.scanresulthistory);

            String split=data.get(position).getDatetime();
            String getdate[]=split.split("T");
            String datagetted=getdate[0];
            String dateorder[]=datagetted.split("-");

            datehistory.setText(dateorder[2]+"/"+dateorder[1]+"/"+dateorder[0]);
            pnumberhistory.setText(data.get(position).getSerialNo());
            scanresulthistory.setText(data.get(position).getScanStatus());

        }
        return convertView;
    }

    @Override
    public int getCount() {
        return data.size();
    }
}
