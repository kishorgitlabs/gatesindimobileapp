package com.brainmagic.gatescatalog.activities.productsearch.fragmentoe;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.brainmagic.gatescatalog.adapter.threeepandablelist.ParentMakeExAdapter;
import com.brainmagic.gatescatalog.api.models.moredetails.ModelDetailsResult;
import com.brainmagic.gatescatalog.gates.R;

import java.util.List;

public class ModelSuitFragment extends Fragment {
    ExpandableListView expandableListView;
    private List<ModelDetailsResult> data;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_videos, container, false);

        expandableListView = view.findViewById(R.id.expandableListView_Parent);
        LinearLayout errorLayout = view.findViewById(R.id.error_layout);
        if (data != null) {
            if (data.size() != 0) {
                ParentMakeExAdapter exAdapter = new ParentMakeExAdapter(getActivity(), data);
                expandableListView.setAdapter(exAdapter);
            } else {
                expandableListView.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
            }
        } else {
            expandableListView.setVisibility(View.GONE);
            errorLayout.setVisibility(View.VISIBLE);
        }

        return view;
    }

    public void setSuitsData(List<ModelDetailsResult> data) {
        this.data = data;
    }
}