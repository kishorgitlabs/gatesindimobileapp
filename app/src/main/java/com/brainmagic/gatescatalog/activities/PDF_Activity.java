package com.brainmagic.gatescatalog.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;

import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.adapter.PDFListViewAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.pdfmodel.PDFModels;
import com.brainmagic.gatescatalog.api.models.pdfmodel.PDFResult;
import com.brainmagic.gatescatalog.font.FontDesign;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PDF_Activity extends Activity {

    ImageView menu;
    ListView listview;
    FloatingActionButton back,home;

    Connection connection;
    Statement stmt;
    ProgressDialog progressDialog;
    ResultSet resultSet;

    ArrayList<String> category;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    String Email, Mobno,Usertype;
    private FontDesign header;
    private Alertbox alertBox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video__category_);

        back=findViewById(R.id.back);
        home=findViewById(R.id.home);
        header=findViewById(R.id.flowtext);
        menu=(ImageView)findViewById(R.id.menu);
        alertBox = new Alertbox(this);

        listview=(ListView)findViewById(R.id.listview);

        category=new ArrayList<>();

        myshare = getSharedPreferences("Registration",MODE_PRIVATE);
        editor = myshare.edit();

        Email=myshare.getString("Email","");
        Mobno=myshare.getString("MobNo","");
        Usertype=myshare.getString("Usertype","");

        header.setText(getIntent().getStringExtra("CATEGORY"));



        checkinternet();

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                PDFResult pdfResult = (PDFResult) adapterView.getAdapter().getItem(i);
                try {
                    if(pdfResult.getPdfURL()!=null) {
                        Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse(pdfResult.getPdfURL()));
                        if (searchAddress != null && !TextUtils.isEmpty(pdfResult.getPdfURL()))
                            startActivity(searchAddress);
                        else {
                            alertBox.showAlert("No Link Found");
                        }
                    }
                }catch (ActivityNotFoundException e)
                {
                    alertBox.showAlert("No Link found to Open");
//                    Snackbar snackbar1 = Snackbar.make(holder.coordinatorLayout,"No Link found to Open", Snackbar.LENGTH_SHORT);
//                    snackbar1.show();
                }
                catch (Exception e)
                {
                    alertBox.showAlert("Invalid Link");
//                    Snackbar snackbar1 = Snackbar.make(holder.coordinatorLayout, "Invalid Link", Snackbar.LENGTH_SHORT);
//                    snackbar1.show();
//                    Log.d(TAG, "onClick: "+e);
                }
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PDF_Activity.this, HomeScreenActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK)
                );
            }
        });


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(PDF_Activity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(PDF_Activity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(PDF_Activity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(PDF_Activity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(PDF_Activity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Video_Category_Activity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.scan:
                                startActivity(new Intent(PDF_Activity.this, ScanActivity.class));
                                return true;

                            case R.id.pricelist:
                                startActivity(new Intent(PDF_Activity.this, Price_Activity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(PDF_Activity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(PDF_Activity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(PDF_Activity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(PDF_Activity.this, Distributor_Search_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Video_Category_Activity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(PDF_Activity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });


    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }

    private void checkinternet() {
        Network_Connection_Activity connection = new Network_Connection_Activity(PDF_Activity.this);

        if (connection.CheckInternet()) {
//            new BackroundRunning().execute();
            callApiForPDF();
        } else {
           // Toast.makeText(Video_Category_Activity.this, "Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            alertBox.showAlertWithBack(" Unable to connect Internet. please check your Internet connection ! ");
        }
    }

    private void callApiForPDF()
    {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(PDF_Activity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetrofitClient.getApiService();
            Call<PDFModels> call = service.getPDFList();

            call.enqueue(new Callback<PDFModels>() {
                @Override
                public void onResponse(Call<PDFModels> call, Response<PDFModels> response) {
                    progressDialog.dismiss();

                    try{
                        if("Success".equals(response.body().getResult()))
                        {

                            listview.setAdapter(new PDFListViewAdapter(PDF_Activity.this
                                    ,response.body().getData()));
//                            listview.setAdapter(product_details);
                        }
                        else {
                            alertBox.showAlertWithBack("No Record Found");
                        }
                    }catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        alertBox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<PDFModels> call, Throwable t) {
                    progressDialog.dismiss();
                    alertBox.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alertBox.showAlertWithBack("Exception Occurred. Please try again Later");
        }
    }


//    class BackroundRunning extends AsyncTask<String, Void, String> {
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//            progressDialog = new ProgressDialog(Video_Category_Activity.this);
//            // spinner (wheel) style dialog
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            progressDialog.setCancelable(false);
//            progressDialog.setMessage("Loading...");
//            progressDialog.show();
//            //   s1=mobilenumber.getText().toString();
//            //  s2=password.getText().toString();
//
//        }
//
//
//
//
//    @Override
//        protected String doInBackground(String... strings) {
//
//            Server_Connection_Activity server=new Server_Connection_Activity();
//            try {
//                connection=server.getconnection();
//
//                stmt=connection.createStatement();
//
//                String selectquery= "SELECT distinct VideoCatagoryname from Video where Activevideo='Active' and deleteflag='notdelete' and VideoCatagoryname !='Catalogues'";
//
//
//                resultSet = stmt.executeQuery(selectquery);
//
//
//                while (resultSet.next()) {
//                    category.add(resultSet.getString("VideoCatagoryname"));
//                }
//
//                if(category.isEmpty())
//                {
//                    return "empty";
//                }
//                resultSet.close();
//                connection.close();
//                stmt.close();
//
//                return "Sucess";
//
//
//
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                return "error";
//            }
//
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//            if (s.equals("Sucess")) {
//
//                listview.setAdapter(new ListViewAdapter(Video_Category_Activity.this
//                        ,category ));
//                progressDialog.dismiss();
//
//            }
//            else if(s.equals("empty"))
//            {
//                showdialog("Sorry! No videos available");
//            }
//
//
//            else {
//                showdialog(" Unable to connect Internet. please check your Internet connection ! ");
//            }
//        }
//    }






//    private void setListView()
//    {
//        category.add("Gates Brochure");
//        category.add("Gates India Brochure");
//        listview.setAdapter(new ListViewAdapter(PDF_Activity.this
//                        ,category ));
//    }




}


