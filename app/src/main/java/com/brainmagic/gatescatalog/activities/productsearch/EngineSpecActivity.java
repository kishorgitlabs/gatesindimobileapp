package com.brainmagic.gatescatalog.activities.productsearch;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.transition.ChangeBounds;
import androidx.transition.TransitionManager;


import com.brainmagic.gatescatalog.activities.Price_Activity;
import com.bumptech.glide.Glide;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.adapter.EngineSpecAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.moredetails.EngineCodeModel;
import com.brainmagic.gatescatalog.api.models.moredetails.EngineCodeResult;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EngineSpecActivity extends Activity
//        implements Product_Details_Adapter.SelectModel
{
    private TextView navicationtxt;
    private List<String> modelStr;
    private String modelstng, modelcodestng, oemstng, partnostng, enginecodestng, yearfromstng, yeartostng, vehicle_type, makeStr, Vehicletype;
    //    private TextView partdescriptiontxt, partnumbertxt, equipment1txt, equipment2txt;
    private Button moreinfo;
    private ImageView PdfDownload;
    private ImageView backArrow;
    private CheckBox mCheckbox;
    Alertbox alertbox;
    //    private CheckBox mCheckboxheader;
    private SQLiteDatabase db;
    private Cursor c;
    //    private ArrayList<String> Segment;
    private ArrayList<String> newMoreDetailsCount, Enginecode, year_fromList, year_tillList, month_fromList, month_tillList, ImageList, partDescriptionList, gates_Part_List, equipmentDateFromList, equipmentDateToList, appAttList, strokeList, ccList, kwList;
    private ImageView menuImg;
    private ProgressDialog loading;
    //    private Product_Details_Adapter adapter;
    private ListView listView;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String Email, Mobno, Usertype, Country;
    private Alertbox box = new Alertbox(EngineSpecActivity.this);
    private int CheckVal = -1;
    public static List<String> userSelection = new ArrayList<>();
    public static ActionMode actionMode = null;
    public static boolean isActionmode = false;
    private Network_Connection_Activity network_connection_activity;
    private TextView ProfileName;
    //    private ConstraintLayout signout_relativelayout;
//    private List<Partnumber> selectModeList;
//    private PdfForamt pdfGenerator;
    private LinearLayout signout_relativelayout;
    private RelativeLayout evenSpace;
    private FloatingActionButton back, home;
    private ViewGroup includeviewLayout;
    private ImageView profilePicture;
    private List<EngineCodeResult> data;
//    private String equipment1srng,equipment2srng;

    String modelCode, engineCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_engine_spec);
        loading = new ProgressDialog(this);
        listView = (ListView) findViewById(R.id.list_view);
        navicationtxt = (TextView) findViewById(R.id.navicationtxt);
        evenSpace = findViewById(R.id.even_space);
        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        network_connection_activity = new Network_Connection_Activity(EngineSpecActivity.this);
        menuImg = (ImageView) findViewById(R.id.menu);
        alertbox = new Alertbox(EngineSpecActivity.this);
        includeviewLayout = findViewById(R.id.profile_logo);
        ProfileName = (TextView) includeviewLayout.findViewById(R.id.profilename);
        signout_relativelayout = findViewById(R.id.signout);
        profilePicture = (ImageView) includeviewLayout.findViewById(R.id.profilepicture);
        signout_relativelayout.setVisibility(View.GONE);
        //  mCheckboxheader = (CheckBox)findViewById(R.id.checkboxmodel1);
        backArrow = (ImageView) findViewById(R.id.backarrow);
//        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
//        listView.setMultiChoiceModeListener(modeListener);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        Vehicletype = getIntent().getStringExtra("segment");
//        Email = myshare.getString("email", "");
//        if(CheckVal ==0){
//            isActionmode = false;
//            actionMode =null;
//            userSelection.clear();
//        }
//        Usertype = myshare.getString("Usertype", "");
//        Country = myshare.getString("Country", "");
        ProfileName.setText(myshare.getString("name", "") + " , " + Country);
        if (!myshare.getString("profile_path", "").equals(""))
            Glide.with(EngineSpecActivity.this)
                    .load(new File(myshare.getString("profile_path", "")))
                    .into(profilePicture);

        if (myshare.getBoolean("islogin", false))
            signout_relativelayout.setVisibility(View.VISIBLE);
        else
            signout_relativelayout.setVisibility(View.GONE);

        signout_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean("islogin", false).commit();
                final Snackbar mySnackbar = Snackbar.make(findViewById(R.id.Product_details_Activity), getString(R.string.channel_network_sign_out), Snackbar.LENGTH_INDEFINITE);
                mySnackbar.setActionTextColor(getResources().getColor(R.color.red));
                mySnackbar.setAction(getString(R.string.okay_button), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mySnackbar.dismiss();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                });
                mySnackbar.show();
            }
        });


        //else post the list

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isActionmode = false;
                actionMode = null;
                // new Vechiclename().execute();
                mCheckbox.setVisibility(View.GONE);
                // mCheckboxheader.setVisibility(View.GONE);
                backArrow.setVisibility(View.GONE);
                mCheckbox.setChecked(false);

                menuImg.setVisibility(View.VISIBLE);
                userSelection.clear();
//                adapter.clearIsChecked();
//                adapter.notifyDataSetChanged();
            }
        });

        final ImageView downArrow = includeviewLayout.findViewById(R.id.down_arrow);
        final RelativeLayout userData = includeviewLayout.findViewById(R.id.user_data);
        ViewGroup homeLayout = findViewById(R.id.Product_details_Activity);

        downArrow.setOnClickListener(new View.OnClickListener() {
            boolean visible;

            @Override
            public void onClick(View v) {
                ChangeBounds changeBounds = new ChangeBounds();
                changeBounds.setDuration(600L);
                TransitionManager.beginDelayedTransition(includeviewLayout, changeBounds);
                visible = !visible;
                if (visible)
                    downArrow.animate().rotation(180).setInterpolator(new LinearInterpolator()).setDuration(500);
                else
                    downArrow.animate().rotation(0).setInterpolator(new LinearInterpolator()).setDuration(500);
                userData.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        });

        //Simply
//        if(mCheckbox.isChecked())
//            mCheckbox.toggle();


        navicationtxt.setText(getIntent().getStringExtra("navtxt").toString());
        modelstng = getIntent().getStringExtra("model");
        modelcodestng = getIntent().getStringExtra("modelcode");
        enginecodestng = getIntent().getStringExtra("engine_codeList");
        oemstng = getIntent().getStringExtra("oem");
        modelStr = new ArrayList<>(Arrays.asList(modelcodestng));
        yearfromstng = getIntent().getStringExtra("yearfrom");
        yeartostng = getIntent().getStringExtra("yearto");
        vehicle_type = getIntent().getStringExtra("segment");

//        GetProduct_Deatils();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                checkInternet(position);
                EngineCodeResult result = (EngineCodeResult) parent.getAdapter().getItem(position);
                startActivity(new Intent(EngineSpecActivity.this, Product_Details_Activity.class)
                                .putExtra("navtxt", navicationtxt.getText().toString() + " > " + result.getCC().trim())
                                .putExtra("MODEL", modelstng)
                                .putExtra("OEM", oemstng)
                                .putExtra("modelCode", modelcodestng)
                                .putExtra("engineCode", enginecodestng)
                                .putExtra("segment", vehicle_type)
                                .putExtra("stroke", result.getStroke())
                                .putExtra("cc", result.getCC())
                                .putExtra("kw", result.getKW())
                                .putExtra("level", result.getLevel1())
//                        .putExtra("partNumber",data.get(position).getArticle())
                                .putExtra("yearFrom", yearfromstng)
                                .putExtra("yearTo", yeartostng)
                );
            }
        });


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(EngineSpecActivity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                isActionmode = false;
            }
        });


        menuImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(EngineSpecActivity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(EngineSpecActivity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(EngineSpecActivity.this, Price_Activity.class));
                                return true;
                            case R.id.aboutpop:
                                startActivity(new Intent(EngineSpecActivity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(EngineSpecActivity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(EngineSpecActivity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Product_Details_Activity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(EngineSpecActivity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(EngineSpecActivity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(EngineSpecActivity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(EngineSpecActivity.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Product_Details_Activity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(EngineSpecActivity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Model_List_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();

            }
        });

        checkInternet();

    }

    private void checkInternet() {
        Network_Connection_Activity network_connection_activity = new Network_Connection_Activity(EngineSpecActivity.this);
        if (network_connection_activity.CheckInternet()) {
            getEngineSpecDetailList();
//            getEngineDetail();
        } else {
            box.showAlertWithBack("Unable to connect Internet. Please check your Internet connection !");
        }
    }

   /* private void getEngineDetail() {
        try {

            modelCode = getIntent().getStringExtra("modelCode");
            engineCode = getIntent().getStringExtra("engineCode");


            APIService service = RetrofitClient.getApiService();

            Call<OtherList> call = service.getOtherList(vehicle_type, oemstng, modelstng, modelCode, engineCode, yearfromstng, yeartostng);

            call.enqueue(new Callback<OtherList>() {
                @Override
                public void onResponse(Call<OtherList> call, Response<OtherList> response) {
                    try {

                        if (response.isSuccessful()) {
                            if (response.body().getResult().equals("Success")) {

//                                model.clear();
//                                model.addAll(response.body().getData());

                                EngineSpecficAdapter product_details_adapter = new EngineSpecficAdapter(EngineSpecActivity.this, response.body().getData());
                                listView.setAdapter(product_details_adapter);

                            } else {
                                box.showAlertWithBack("No Record Found");
                            }
                        } else {
                            box.showAlertWithBack("Could not Connect to Server. Please try again Later");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        box.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }


                @Override
                public void onFailure(Call<OtherList> call, Throwable t) {
                    box.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            box.showAlertWithBack("Exception Occurred. Please try again Later");
        }

    }*/


    private void getEngineSpecDetailList() {
        try {

            String vehicleType = getIntent().getStringExtra("segment");

            /*if (vehicleType.equals("Heavy Commercial") || vehicleType.equals("2 Wheeler")){
//                modelCode = getIntent().getStringExtra("modelCode2WHCV");
                yearfromstng = "NA";
                yeartostng = "NA";
            }else {

                yeartostng = "";
                yearfromstng = "";

            }*/

                modelCode = getIntent().getStringExtra("modelCode");
//            }
            engineCode = getIntent().getStringExtra("engineCode");


            APIService service = RetrofitClient.getApiService();

            Call<EngineCodeModel> call = service.getEngineSpecList(vehicle_type, oemstng, modelstng, modelCode, engineCode, yearfromstng, yeartostng);

            call.enqueue(new Callback<EngineCodeModel>() {
                @Override
                public void onResponse(Call<EngineCodeModel> call, Response<EngineCodeModel> response) {
                    try {

                        if (response.isSuccessful()) {
                            if (response.body().getResult().equals("Success")) {

//                                model.clear();
//                                model.addAll(response.body().getData());

                                EngineSpecAdapter product_details_adapter = new EngineSpecAdapter(EngineSpecActivity.this, response.body().getData());
                                listView.setAdapter(product_details_adapter);

                            } else if (response.body().getResult().equals("No Record")){
                                box.showAlertWithBack("No Record Found");
                            }
                        } else {
                            box.showAlertWithBack("Could not Connect to Server. Please try again Later");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        box.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }


                @Override
                public void onFailure(Call<EngineCodeModel> call, Throwable t) {
                    box.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            box.showAlertWithBack("Exception Occurred. Please try again Later");
        }

    }


    private void checkInternet(int p) {
        Network_Connection_Activity net = new Network_Connection_Activity(EngineSpecActivity.this);
        if (net.CheckInternet()) {
            startActivity(p);
        } else {
//            Intent a = new Intent(Product_Details_Activity.this, Product_More_Details_Activity.class);
            Intent a = new Intent(EngineSpecActivity.this, MoreDetailsActivity.class);
            a.putExtra("MODEL_CODE", modelcodestng);
            a.putExtra("PARTDESC", partDescriptionList.get(p));
            a.putExtra("GATESPART", gates_Part_List.get(p));
            a.putExtra("ENGINE_CODE", enginecodestng);
            a.putExtra("APPATT", appAttList.get(p));
            a.putExtra("STROKE", strokeList.get(p));
            a.putExtra("CC", ccList.get(p));
            a.putExtra("KW", kwList.get(p));
            a.putExtra("yearfrom", year_fromList.get(p).toString());
            a.putExtra("yearto", year_tillList.get(p).toString());
            //   a.putExtra("EQUIPMENTDATET", equipmentDateToList.get(position));
            //  a.putExtra("IMAGENAME", ImageList.get(position));
            a.putExtra("navtxt", navicationtxt.getText().toString());
            a.putExtra("MODEL", modelstng);
            a.putExtra("OEM", oemstng);
            a.putExtra("thumbuimage", (Serializable) null);
            a.putExtra("internet", "Nointernet");
            startActivity(a);
        }
    }

//    @Override
//    public void selectedModel(List<Partnumber> partnumbers) {
//        if(!(partnumbers.size()==0)) {
//            Model model = new Model();
//            model.setModelpdf(modelstng);
//            List<Model> modelLists = new ArrayList<>();
//            modelLists.add(model);
//            pdfGenerator.setMake(oemstng);
//            pdfGenerator.setSegment(Vehicletype);
//            pdfGenerator.setModel(modelLists);
//            pdfGenerator.setEngine(enginecodestng);
//            this.selectModeList = partnumbers;
//            pdfGenerator.setPartnumber(selectModeList);
//            pdfGenerator.setModelCode(modelcodestng);
//            if(Country.equals("All Countries"))
//                pdfGenerator.setCountry("All");
//            else
//                pdfGenerator.setCountry(Country);
//        }else
//            pdfGenerator.setPartnumber(null);
//
//
//    }

    private void startActivity(int pa) {

//        try {
//            String Partnumber = gates_Part_List.get(pa);
//
//            loading = ProgressDialog.show(Product_Details_Activity.this,"",getString(R.string.loadings),false,false);
//
//            APIServices service = RetroClient.getApiService();
//
//            Call<ThumbImage> call = service.MASTER_IMAGE_CALL(Partnumber);
//            call.enqueue(new Callback<ThumbImage>() {
//                @Override
//                public void onResponse(Call<ThumbImage> call, Response<ThumbImage> response) {
//                    if (response.body().getResult().equals("Success")) {
        List<Integer> imageList = null;
//                        imageList.add(R.drawable);
//                        loading.dismiss();
        Intent a = new Intent(EngineSpecActivity.this, MoreDetailsActivity.class);
        a.putExtra("MODEL_CODE", modelcodestng);
        a.putExtra("MODEL", modelstng);
        a.putExtra("OEM", oemstng);
        a.putExtra("PARTDESC", partDescriptionList.get(pa));
        a.putExtra("GATESPART", gates_Part_List.get(pa));
        a.putExtra("ENGINE_CODE", enginecodestng);
        a.putExtra("APPATT", appAttList.get(pa));
        a.putExtra("STROKE", strokeList.get(pa));
        a.putExtra("CC", ccList.get(pa));
        a.putExtra("KW", kwList.get(pa));
        a.putExtra("yearfrom", year_fromList.get(pa).toString());
        a.putExtra("yearto", year_tillList.get(pa).toString());
        a.putExtra("navtxt", navicationtxt.getText().toString());
        a.putExtra("thumbuimage", (Serializable) imageList);
        a.putExtra("internet", "Success");
        startActivity(a);
//                    } else {
//                        //Toast.makeText(getApplicationContext(), "Error in Fetch CountDetails", Toast.LENGTH_LONG).show();
//                        loading.dismiss();
//                        Intent a = new Intent(Product_Details_Activity.this, Product_More_Details_Activity.class);
//                        a.putExtra("MODEL_CODE", modelcodestng);
//                        a.putExtra("MODEL", modelstng);
//                        a.putExtra("OEM", oemstng);
//                        a.putExtra("PARTDESC", partDescriptionList.get(pa));
//                        a.putExtra("GATESPART", gates_Part_List.get(pa));
//                        a.putExtra("ENGINE_CODE", enginecodestng);
//                        a.putExtra("EQUIPMENT1", equipment1List.get(pa));
//                        a.putExtra("EQUIPMENT2", equipment2List.get(pa));
//                        a.putExtra("yearfrom", year_fromList.get(pa).toString());
//                        a.putExtra("yearto", year_tillList.get(pa).toString());
//                        a.putExtra("navtxt", navicationtxt.getText().toString());
//                        a.putExtra("thumbuimage",(Serializable) null);
//                        a.putExtra("internet","Nointernet");
//                        startActivity(a);
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ThumbImage> call, Throwable t) {
//                    Intent a = new Intent(Product_Details_Activity.this, Product_More_Details_Activity.class);
//                    a.putExtra("MODEL_CODE", modelcodestng);
//                    a.putExtra("MODEL", modelstng);
//                    a.putExtra("OEM", oemstng);
//                    a.putExtra("PARTDESC", partDescriptionList.get(pa));
//                    a.putExtra("GATESPART", gates_Part_List.get(pa));
//                    a.putExtra("ENGINE_CODE", enginecodestng);
//                    a.putExtra("EQUIPMENT1", equipment1List.get(pa));
//                    a.putExtra("EQUIPMENT2", equipment2List.get(pa));
//                    a.putExtra("yearfrom", year_fromList.get(pa).toString());
//                    a.putExtra("yearto", year_tillList.get(pa).toString());
//                    a.putExtra("navtxt", navicationtxt.getText().toString());
//                    a.putExtra("thumbuimage",(Serializable) null);
//                    a.putExtra("internet","Nointernet");
//                    startActivity(a);
//                    loading.dismiss();
//
//
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//            Intent a = new Intent(Product_Details_Activity.this, Product_More_Details_Activity.class);
//            a.putExtra("MODEL_CODE", modelcodestng);
//            a.putExtra("MODEL", modelstng);
//            a.putExtra("OEM", oemstng);
//            a.putExtra("PARTDESC", partDescriptionList.get(pa));
//            a.putExtra("GATESPART", gates_Part_List.get(pa));
//            a.putExtra("ENGINE_CODE", enginecodestng);
//            a.putExtra("EQUIPMENT1", equipment1List.get(pa));
//            a.putExtra("EQUIPMENT2", equipment2List.get(pa));
//            a.putExtra("yearfrom", year_fromList.get(pa).toString());
//            a.putExtra("yearto", year_tillList.get(pa).toString());
//            a.putExtra("navtxt", navicationtxt.getText().toString());
//            a.putExtra("thumbuimage",(Serializable) null);
//            a.putExtra("internet","Nointernet");
//            startActivity(a);
//            loading.dismiss();
//
//        }
    }

    private void getPdf() {

//        try {
//            final ProgressDialog progressDialog = new ProgressDialog(Product_Details_Activity.this);
//            progressDialog.setIndeterminate(true);
//            progressDialog.setMessage(getString(R.string.loadings));
//            progressDialog.setCancelable(false);
//
//            progressDialog.show();
//            APIServices service = RetroClient.getApiService();
//
//            Call<PdfFormatResult> call = service.generatePDF(pdfGenerator);
//            call.enqueue(new Callback<PdfFormatResult>() {
//                @Override
//                public void onResponse(Call<PdfFormatResult> call, Response<PdfFormatResult> response) {
//                    if (response.body().getResult().equals("Success")) {
//                        //  SetStateData(response.body());
//                        pdfGenerator.setPartnumber(null);
//                        try {
//
//                            if(response.body().getData()!=null){
//                                Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().getData()));
//                                if(searchAddress!=null) {
//                                    startActivity(searchAddress);
//                                    CheckVal = 0;
//                                }
//                            }
//                        }catch (ActivityNotFoundException e)
//                        {
//
//                            alertbox.showAlert(getString(R.string.pdf_not_found));
//                            /*Snackbar snackbar1 = Snackbar.make(coordinatorLayout, "PDF is not found", Snackbar.LENGTH_SHORT);
//                            snackbar1.show();*/
//                        }
//                        catch (Exception e)
//                        {
//                            alertbox.showAlert(getString(R.string.invalid_link));
//                           /* Snackbar snackbar1 = Snackbar.make(holder.coordinatorLayout, "Invalid Link", Snackbar.LENGTH_SHORT);
//                            snackbar1.show();*/
//                        }
//
//
//
//                        progressDialog.dismiss();
//
//                    } else {
//                        alertbox.showAlert(getString(R.string.failed_to_fetch));
//                        // android.widget.Toast.makeText(getApplicationContext(), "Failed to fetch data from the server.Please contact admin", android.widget.Toast.LENGTH_LONG).show();
//                        progressDialog.dismiss();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<PdfFormatResult> call, Throwable t) {
//                    alertbox.showAlert(getString(R.string.server_not_responding));
//                    // android.widget.Toast.makeText(getApplicationContext(), " Server is not responding", android.widget.Toast.LENGTH_LONG).show();
//                    progressDialog.dismiss();
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//            alertbox.showAlert(getString(R.string.fetching_incorrect_data));
//            //  android.widget.Toast.makeText(getApplicationContext(), " Fetching incorrect data", android.widget.Toast.LENGTH_LONG).show();
//
//        }

    }


    @Override
    protected void onResume() {
        super.onResume();

//        if (myshare.getBoolean("islogin", false))
//            signout_relativelayout.setVisibility(View.VISIBLE);
//        else
//            signout_relativelayout.setVisibility(View.GONE);
//        userSelection.clear();
//        actionMode = null;
//        mCheckbox.setVisibility(View.GONE);
//        //  PdfDownload.setVisibility(View.GONE);
//        backArrow.setVisibility(View.GONE);
//        menuImg.setVisibility(View.VISIBLE);
//        mCheckbox.setChecked(false);
//        pdfGenerator.setPartnumber(null);
//        isActionmode = false;
//        if(adapter!=null) {
//            adapter.clearIsChecked();
//            adapter.notifyDataSetChanged();
//        }
    }

}
