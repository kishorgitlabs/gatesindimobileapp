package com.brainmagic.gatescatalog.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.Nullable;

import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.logdetail.LogDetailModel;
import com.brainmagic.gatescatalog.api.models.sendlog.SendLog;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BackgroundService extends IntentService {
    public BackgroundService() {
        super("");
    }

    public BackgroundService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        try {
            Network_Connection_Activity connection = new Network_Connection_Activity(getApplicationContext());
            if (connection.CheckInternet()) {

                String segment = intent.getStringExtra("segmentLog");
                String name = intent.getStringExtra("nameLog");
                String vehicleName = intent.getStringExtra("vehicleNameLog");
                String partNo = intent.getStringExtra("partNoLog");
                String mobileNo = intent.getStringExtra("mobileNoLog");
                String userType = intent.getStringExtra("userTypeLog");
                String state = intent.getStringExtra("stateLog");
                String city = intent.getStringExtra("cityLog");
                String zipCode = intent.getStringExtra("zipCodeLog");
                String location = intent.getStringExtra("locationLog");
                String deviceType = intent.getStringExtra("deviceTypeLog");


                APIService service = RetrofitClient.getApiQualityService();

                Call<SendLog> call = service.sendLog(segment, name, vehicleName, partNo, mobileNo, userType,
                        state, city, zipCode, location, deviceType);

                call.enqueue(new Callback<SendLog>() {
                    @Override
                    public void onResponse(Call<SendLog> call, Response<SendLog> response) {
                        if (response.isSuccessful())
                            stopSelf();
                    }

                    @Override
                    public void onFailure(Call<SendLog> call, Throwable t) {
                        stopSelf();
                    }
                });

//            Response<String> res = call.execute();

//                call.enqueue(new Callback<LogDetailModel>() {
//                    @Override
//                    public void onResponse(Call<LogDetailModel> call, Response<LogDetailModel> response) {
////                    if(response.isSuccessful())
//                        stopSelf();
//                    }
//
//                    @Override
//                    public void onFailure(Call<LogDetailModel> call, Throwable t) {
//                        stopSelf();
//                    }
//                });


            }
        } catch (Exception e) {
            e.printStackTrace();
            stopSelf();
        }

        return START_REDELIVER_INTENT;
    }
}
