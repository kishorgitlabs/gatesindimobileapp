package com.brainmagic.gatescatalog.api;

import com.brainmagic.gatescatalog.api.models.PriceList.OePartListPojo;
import com.brainmagic.gatescatalog.api.models.PriceList.PricePojo;
import com.brainmagic.gatescatalog.api.models.PriceList.ProductOePojo;
import com.brainmagic.gatescatalog.api.models.StringListModel;
import com.brainmagic.gatescatalog.api.models.VideosMoreDetails.TrainingMoreDetailsModel;
import com.brainmagic.gatescatalog.api.models.checkregister.CheckRegister;
import com.brainmagic.gatescatalog.api.models.distributors.DistributorsListModel;
import com.brainmagic.gatescatalog.api.models.moredetails.EngineCodeModel;
import com.brainmagic.gatescatalog.api.models.imageupload.UploadImage;
import com.brainmagic.gatescatalog.api.models.moredetails.MoreDetailsModel;
import com.brainmagic.gatescatalog.api.models.notification.NotificationModel;
import com.brainmagic.gatescatalog.api.models.oesearchpartno.OESearchPartNo;
import com.brainmagic.gatescatalog.api.models.otherlists.OtherList;
import com.brainmagic.gatescatalog.api.models.pdfmodel.PDFModels;
import com.brainmagic.gatescatalog.api.models.registration.Registration;
import com.brainmagic.gatescatalog.api.models.sacnhistory.ScanHistoryList;
import com.brainmagic.gatescatalog.api.models.scanresult.ScanResultReward;
import com.brainmagic.gatescatalog.api.models.scanverify.gatesemployeeapproval.Approval;
import com.brainmagic.gatescatalog.api.models.scanverify.olduserverify.OldUserVerify;
import com.brainmagic.gatescatalog.api.models.scanverify.verifieddata.VerifiedData;
import com.brainmagic.gatescatalog.api.models.schemespromo.SchemesPromoModel;
import com.brainmagic.gatescatalog.api.models.sendlog.SendLog;
import com.brainmagic.gatescatalog.api.models.sendotp.SendOTP;
import com.brainmagic.gatescatalog.api.models.trainingvideos.TrainingVideosModel;
import com.brainmagic.gatescatalog.api.models.uploaduserdetails.UploadUserdetails;
import com.brainmagic.gatescatalog.api.models.webotpverify.WebOTPVerify;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIService {

    @FormUrlEncoded
    @POST("api/Values/webOtpverifiy")
    public Call<WebOTPVerify> webverify(
            @Field("MobileNo") String mobilenumber,
            @Field("UserType") String utype
    );

    @FormUrlEncoded
    @POST("api/Values/SendOTP")
    public Call<SendOTP> sendtop(
            @Field("MobileNo") String mobilenumber,
            @Field("name") String name,
            @Field("UserType") String utype,
            @Field("Emailid") String em,
            @Field("BusinessName") String bn,
            @Field("Address") String ad
    );

    @FormUrlEncoded
    @POST("api/Values/CheckReg")
    public Call<CheckRegister> checkreg(
            @Field("MobileNo") String mobilenumber
    );

    @FormUrlEncoded
    @POST("api/Values/NewRegister")
    public Call<Registration> newregister(
            @Field("usertype") String utype,
            @Field("MobileNo") String mob,
            @Field("Status") String status,
            @Field("name") String name,
            @Field("email") String email,
            @Field("appid") String apid,
            @Field("OTPStatus") String otpstatu,
            @Field("Date") String date,
            @Field("Password") String pass,
            @Field("Country") String country,
            @Field("OTP") String otp,
            @Field("BusinessName") String bnam,
            @Field("Latitude") String lat,
            @Field("Longitude") String longi,
            @Field("Address") String addd,
            @Field("State") String state,
            @Field("City") String city,
            @Field("Zipcode") String zipCode
    );

    @FormUrlEncoded
    @POST("api/Values/ScanPart")
//    @POST("api/Values/ScanPartTest")
    public Call<ScanResultReward> scan(
            @Field("MobileNo") String mobilenumber,
            @Field("SerialNo") String serial,
            @Field("usertype") String utype,
            @Field("BusinessName") String bname,
            @Field("Address") String add,
            @Field("Latitude") String lat,
            @Field("Longitude") String lon
    );

    @Multipart
    @POST("api/Upload/PostUserImage")
    public Call<UploadImage> imageupload(
            @Part MultipartBody.Part file
    );

    @FormUrlEncoded
    @POST("api/Values/gatesImage")
    public Call<UploadUserdetails> gatesimageuserdetails(
            @Field("MobileNo") String mobilenumber,
            @Field("Image") String img,
            @Field("Location") String loc
    );

    @GET("api/Values/Notification")
    public Call<NotificationModel> getNofication(
    );

    @GET("api/Values/Getwhatsnew")
    public Call<EngineCodeModel> getProductDetailWhatsNew(
    );

    @FormUrlEncoded
    @POST("api/Values/Scanhistory")
    public Call<ScanHistoryList> sacnhistory(
            @Field("MobileNo") String mobilenumber
    );


    @FormUrlEncoded
    @POST("api/Values/pricelist")
    public Call<PricePojo> prices(
            @Field("Partno") String pricelists
    );


    @GET("api/Values/Trainingvideos")
    public Call<TrainingVideosModel> getTrainingVideos();

    @FormUrlEncoded
    @POST("api/Values/postvideo")
    public Call<TrainingMoreDetailsModel> postVideo(
            @Field("VideoCatagoryname") String videoName
    );


    @GET("api/Values/Otherlinks")
    public Call<TrainingMoreDetailsModel> otherLinks();

    @GET("api/Values/Distributorname")
    public Call<StringListModel> getDistributorNameList();

    @FormUrlEncoded
    @POST("api/Values/Distributordetail")
    public Call<DistributorsListModel> getDistributorList(
            @Field("DistributorKey") String name
    );

    @GET("api/Values/GetCountry")
    public Call<StringListModel> getDistributorCountryList();

    @FormUrlEncoded
    @POST("api/Values/GetState")
    public Call<StringListModel> getDistributorStateList(
            @Field("CountryName") String country
    );

    @FormUrlEncoded
    @POST("api/Values/GetCity")
    public Call<StringListModel> getDistributorCityList(
            @Field("CountryName") String country,
            @Field("State") String state
    );

    @FormUrlEncoded
    @POST("api/Values/Distributorsearch")
    public Call<DistributorsListModel> getDistributorList(
            @Field("Country") String country,
            @Field("State") String state,
            @Field("City") String city
    );

    @FormUrlEncoded
    @POST("api/Values/getmake")
    public Call<StringListModel> getMakeList(
            @Field("Segment") String segment
    );

    @FormUrlEncoded
    @POST("api/Values/getmodel")
    public Call<StringListModel> getModelList(
            @Field("Segment") String segment,
            @Field("Make") String make
    );

    @FormUrlEncoded
    @POST("api/Values/getenginecode")
    public Call<EngineCodeModel> getEngineList(
            @Field("Segment") String segment,
            @Field("Make") String make,
            @Field("model") String model
    );

    @FormUrlEncoded
    @POST("api/Values/Otherlist")
    public Call<EngineCodeModel> getEngineSpecList(
            @Field("Segment") String segment,
            @Field("Make") String make,
            @Field("Model") String model,
            @Field("ModelCode") String modelCode,
            @Field("EngineCode") String engineCode,
            @Field("YearFrom") String yearFrom,
            @Field("YearTill") String yearTill
    );

    @FormUrlEncoded
    @POST("api/Values/Otherlist")
    public Call<OtherList> getOtherList(
            @Field("Segment") String segment,
            @Field("Make") String make,
            @Field("Model") String model,
            @Field("Modelcode") String modelCode,
            @Field("Enginecode") String engineCode,
            @Field("YearFrom") String yearFrom,
            @Field("YearTill") String yearTill
    );

    @FormUrlEncoded
    @POST("api/Values/GetenginecodeFilter")
    public Call<EngineCodeModel> getProductDetailList(
            @Field("Segment") String segment,
            @Field("Make") String make,
            @Field("model") String model,
            @Field("engineCode") String engineCode,
            @Field("modelCode") String modelCode,
            @Field("YearFrom") String yearFrom,
            @Field("YearTill") String yearTill,
            @Field("stroke") double stroke,
            @Field("cc") String cc,
            @Field("kw") String kw,
            @Field("level1") String level
    );

    @FormUrlEncoded
    @POST("api/Values/Whatsnewmore")
    public Call<MoreDetailsModel> getProductMoreDetailList(
            @Field("Article") String gatePartNumber
    );

    @FormUrlEncoded
    @POST("api/Values/OEdetailsmore")
    public Call<MoreDetailsModel> getOEProductMoreDetailList(
            @Field("OEnumber") String PartNumber
    );

    @FormUrlEncoded
    @POST("api/Values/Productdetail")
    public Call<MoreDetailsModel> getProductSearchMoreDetailList(
            @Field("Segment") String segment,
            @Field("Make") String make,
            @Field("model") String model,
            @Field("engineCode") String engineCode,
            @Field("modelCode") String modelCode,
            @Field("YearFrom") String yearFrom,
            @Field("YearTill") String yearTill,
            @Field("stroke") double stroke,
            @Field("cc") String cc,
            @Field("kw") String kw,
            @Field("level1") String level,
            @Field("Article") String article
    );

    @GET("api/Values/OtherlinkPDF")
    public Call<PDFModels> getPDFList();

    @FormUrlEncoded
    @POST("api/Values/Getschemes")
    public Call<SchemesPromoModel> getSchemesPromo(
            @Field("Usertype") String userType
    );

    /*@FormUrlEncoded
    @POST("api/Values/LogDetail")
    public Call<LogDetailModel> sendLog(
            @Field("UserLogin") String mobile,
            @Field("Product") String productName,
            @Field("Recordname") String productDetail
    );*/

    @FormUrlEncoded
    @POST("api/Values/LogDetailNew")
    public Call<SendLog> sendLog(
            @Field("Segment") String segment,
            @Field("Name") String name,
            @Field("VehicleModel") String vehicleModel,
            @Field("partno") String partNo,
            @Field("Mobileno") String mobileNo,
            @Field("Usertype") String userType,
            @Field("State") String state,
            @Field("City") String city,
            @Field("Zipcode") String zipCode,
            @Field("Location") String location,
            @Field("Devicetype") String deviceType
    );

    @FormUrlEncoded
    @POST("api/Values/Approve")
    public Call<Approval> approval(
            @Field("MobileNo") String mobilenumber
    );

    @FormUrlEncoded
    @POST("api/Values/ValidateOTP")
    public Call<VerifiedData> verified(
            @Field("MobileNo") String mobilenumber,
            @Field("usertype") String utype

    );

    @FormUrlEncoded
    @POST("api/Values/NewOTPVerify")
    public Call<OldUserVerify> newverify(
            @Field("name") String mobilenumber,
            @Field("MobileNo") String mob,
            @Field("email") String email,
            @Field("usertype") String utype,
            @Field("OTPStatus") String otpstatus,
            @Field("OTP") String otp,
            @Field("BusinessName") String bname,
            @Field("Address") String address
    );

    @FormUrlEncoded
    @POST("api/Values/oesearch")
    public Call<OePartListPojo> oeSearchPartNo(
            @Field("Oenumber") String oeNumber
    );

    @FormUrlEncoded
    @POST("api/Values/oesearch")
    public Call<ProductOePojo> oeSearchPartNos(
            @Field("Oenumber") String oeNumber
    );
}
