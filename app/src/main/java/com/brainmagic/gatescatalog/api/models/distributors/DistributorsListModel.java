
package com.brainmagic.gatescatalog.api.models.distributors;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class DistributorsListModel {

    @SerializedName("data")
    private List<DistributorListResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<DistributorListResult> getData() {
        return mData;
    }

    public void setData(List<DistributorListResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
