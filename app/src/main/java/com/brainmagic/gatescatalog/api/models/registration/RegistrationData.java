
package com.brainmagic.gatescatalog.api.models.registration;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class RegistrationData {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("appid")
    private String mAppid;
    @SerializedName("BusinessName")
    private String mBusinessName;
    @SerializedName("Country")
    private String mCountry;
    @SerializedName("Date")
    private String mDate;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Latitude")
    private String mLatitude;
    @SerializedName("Location")
    private Object mLocation;
    @SerializedName("Longitude")
    private String mLongitude;
    @SerializedName("mobileno")
    private String mMobileno;
    @SerializedName("name")
    private String mName;
    @SerializedName("OTPStatus")
    private String mOTPStatus;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("RegistrationType")
    private String mRegistrationType;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("updatetime")
    private Object mUpdatetime;
    @SerializedName("usertype")
    private String mUsertype;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAppid() {
        return mAppid;
    }

    public void setAppid(String appid) {
        mAppid = appid;
    }

    public String getBusinessName() {
        return mBusinessName;
    }

    public void setBusinessName(String businessName) {
        mBusinessName = businessName;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public Object getLocation() {
        return mLocation;
    }

    public void setLocation(Object location) {
        mLocation = location;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getMobileno() {
        return mMobileno;
    }

    public void setMobileno(String mobileno) {
        mMobileno = mobileno;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getOTPStatus() {
        return mOTPStatus;
    }

    public void setOTPStatus(String oTPStatus) {
        mOTPStatus = oTPStatus;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getRegistrationType() {
        return mRegistrationType;
    }

    public void setRegistrationType(String registrationType) {
        mRegistrationType = registrationType;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Object getUpdatetime() {
        return mUpdatetime;
    }

    public void setUpdatetime(Object updatetime) {
        mUpdatetime = updatetime;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

}
